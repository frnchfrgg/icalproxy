#!/usr/bin/env python3

from http.server import BaseHTTPRequestHandler, ThreadingHTTPServer
import requests, socket, ssl, pathlib, os, json, sys, hashlib
import re, collections
from datetime import datetime, timezone

try:
    CONFFILE = pathlib.Path(os.environ["ICALPROXY_CONFIG"])
except KeyError:
    HOME = pathlib.Path(os.environ.get("HOME", ".")).resolve()
    CONFFILE = HOME / "icalproxyrc"

try:
    CONFIG = json.load(CONFFILE.open())
except:
    CONFIG = {}

CALENDAR_ID = CONFIG.get("calendar_id", "")
REMOTE_URL = CONFIG.get("remote_url", "")

DATE_RANGES = collections.defaultdict(list)
for rtype, spans in CONFIG.get("date_ranges", {}).items():
    for span in spans:
        try:
            DATE_RANGES[rtype].append(
                    (datetime.fromisoformat(span["from"]),
                     datetime.fromisoformat(span["to"])))
        except:
            print(f"Warning: ignored incorrect entry: {span}")

try:
    SUMMARY_REPLACE = CONFIG["replace"]["summary"]
except KeyError:
    SUMMARY_REPLACE = {}

ICALFMT = "%Y%m%dT%H%M%S"

def do_hash(*x):
    return hashlib.sha1(repr(x).encode()).hexdigest()

class ICalProxy():
    def get(self):
        try:
            resp = requests.get(REMOTE_URL, stream = True)
            status_code = resp.status_code
        except:
            status_code = 503

        if not status_code == 200:
            return ""

        if resp.encoding is None: resp.encoding = 'utf-8'

        pending = []
        info = {}
        ical = []

        for line in resp.iter_lines(decode_unicode=True):
            if line == "BEGIN:VEVENT":
                ical.extend(pending)
                pending = [line]
                info = {}
            elif line == "END:VEVENT":
                # Cleanup stupidly unstable IDs and stamps
                pending = [ x for x in pending
                    if not (x.startswith("DTSTAMP:") or x.startswith("UID:")) ]
                if self.check_event(pending, info):
                    event = self.mangle_event(pending, info)
                    uid = do_hash(event)
                    ical.extend(event)
                    ical.append(f"UID:{uid}@icalproxy-{CALENDAR_ID}")
                    if "DTSTART" in info:
                        ical.append(f"DTSTAMP:{info['DTSTART']:{ICALFMT}}")
                    ical.append(line)
                pending = []
            elif line:
                pending.append(line)
                # try lexing the line
                try:
                    key, value = line.split(":", 1)
                except:
                    continue
                value = value.strip()
                key, *optlist = key.upper().split(";")
                key = key.strip()
                options = {}
                for i, opt in enumerate(optlist):
                    o, *v = opt.split("=", 1)
                    options[o.strip()] = v[0].strip() if v else ''
                # parse some keys
                if key == "DTSTART":
                    thetime = value.rstrip("Z")
                    try:
                        thetime = datetime.strptime(thetime, ICALFMT)
                    except ValueError:
                        try:
                            thetime = datetime.strptime(thetime, "%Y%m%d")
                        except ValueError:
                            continue
                    if value.endswith("Z"):
                        thetime = thetime.replace(tzinfo=timezone.utc)
                    else:
                        thetime = thetime.astimezone() # local time
                    info[key] = thetime
                elif key == "SUMMARY":
                    if "LANGUAGE" in options:
                        info[f"{key}_{options['LANGUAGE']}"] = value
                    else:
                        info[key] = value

        ical.extend(pending) # flush content after last event
        if ical and ical[-1]:
            ical.append("") # ensure last CRLF

        return "\r\n".join(ical)

    def check_event(self, event, info):
        summary = (info.get("SUMMARY_FR")
                or info.get("SUMMARY") or "").upper()
        for x in ["COURS ANNUL", "VACANCES", "FÉRIÉ", "ABS "]:
            if summary.startswith(x):
                return False
        if "DTSTART" not in info: return True # in doubt, let it pass
        dtstart = info["DTSTART"]
        if not any(f <= dtstart <= l for f,l in DATE_RANGES["include"]):
            return False
        if any(f <= dtstart <= l for f,l in DATE_RANGES["exclude"]):
            return False
        return True

    def mangle_event(self, event, info):
        for i, l in enumerate(event):
            if l.startswith("SUMMARY"):
                for old, new in SUMMARY_REPLACE.items():
                    l = l.replace(old, new)
                event[i] = l
        return event

if __name__ == "__main__":
    sys.stdout.write(ICalProxy().get())
